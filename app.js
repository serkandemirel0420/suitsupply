//region modules
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//endregion

var article = require('./routes/article');
var blog = require('./routes/blog');

var app = express();
app.use(favicon(__dirname + '/public/favicon.ico'));

var exphbs  = require('express-handlebars');


//region view helpers
var helpers = {
  ifCond: function (v1, operator, v2, options) {

    switch (operator) {
      case '==':
        return (v1 == v2) ? options.fn(this) : options.inverse(this);
      case '===':
        return (v1 === v2) ? options.fn(this) : options.inverse(this);
      case '<':
        return (v1 < v2) ? options.fn(this) : options.inverse(this);
      case '<=':
        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
      case '>':
        return (v1 > v2) ? options.fn(this) : options.inverse(this);
      case '>=':
        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
      case '&&':
        return (v1 && v2) ? options.fn(this) : options.inverse(this);
      case '||':
        return (v1 || v2) ? options.fn(this) : options.inverse(this);
      default:
        return options.inverse(this);
    }
  },
  prettyDate: function(timestamp) {
    return moment(new Date(timestamp)).fromNow();
  }
};
//endregion


//region view engine setup
app.engine('.html', exphbs({extname: '.html', helpers: helpers}));
app.set('view engine', '.html');
app.set('views', __dirname + '/views');
//endregion


app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/article', article);
app.use('/', blog);






//region 404 handler - passes to app error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
//endregion

//region development error handler
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log("Error-c ;" + err);
    res.send({status:"Error Handler"});
  });
}
//endregion


//region production error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
//endregion


module.exports = app;
