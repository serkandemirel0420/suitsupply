var express = require('express');
var router = express.Router();

var validation = require("../validation/index");

var JsonDB = require("../lib/jsonStore");

var DB = new JsonDB();

router.get('/setup', function (req, res, next) {

    var basicConfig = {};
    basicConfig.path = DB.fullMainFilePath;
    basicConfig.content = JSON.stringify(DB.defaultConfigFileContent);


    DB.writeFile(basicConfig)
        .then(function (data) {
            res.json({status: "success", "path": basicConfig.path, "content": basicConfig.content});
        }).catch(function (err) {
            throw Error(err);
        });
    
});

router.get('/all' , function (req, res, next) {

    DB.readFile(DB.fullMainFilePath)
        .then(function (data) {

            var parsedData = JSON.parse( data.content);

            if(parsedData.Docs) return res.json(parsedData.Docs);

            res.status(204).end()


        }).catch(function (err) {
        next(err)
    });

});

router.get('/:id', validation.validateGet(validation.get) , function (req, res, next) {

    DB.readFile(DB.fullMainFilePath)
        .then(function (data) {
            var parsedData = JSON.parse(data.content);
            var result = DB.searchInJsonArr(parsedData.Docs, req.params.id)

            if(result) return res.json(result);

            res.status(204).end()


        }).catch(function (err) {
            next(err)
        });

});

router.put('/',validation.validatePost(validation.put) , function (req, res, next) {
    DB.readFile(DB.fullMainFilePath)
        .then(function (data) {

            var parsedData = JSON.parse(data.content);
            debugger
            var ee = {};
            ee.title = req.body.title;
            ee.body = req.body.body;
            parsedData.Docs = DB.updateJsonArr(parsedData.Docs, req.body.id, ee);

            var newObject = {};
            newObject.path = DB.fullMainFilePath;
            newObject.content = parsedData;
            return newObject;

        })
        .then(DB.writeFile)
        .then(function (data) {

            res.json(data);
            res.end();
            //index after response sent
            DB.index(data.content.Docs);
        })
        .catch(function (err) {
             next(err)
        });

});

router.post('/', validation.validatePost(validation.post) ,function (req, res, next) {

    var validatedData = {id: null, title: req.body.title , body: req.body.body, timestamp: new Date().getTime() };

    DB.readFile(DB.fullMainFilePath)
        .then(function (data) {

            var parsedData = JSON.parse(data.content);
            var uniqueId = DB.uniqueId();
            validatedData.id = uniqueId;
            //add data to top
            parsedData.Docs.unshift(validatedData);


            var newObject = {};
            newObject.path = DB.fullMainFilePath;
            newObject.content = parsedData;
            return newObject;
        })
        .then(DB.writeFile)
        .then(function (data) {

            res.json(data);
            res.end();
            //index after response sent
            DB.index(data.content.Docs);
        })
        .catch(function (err) {
            next(err);
        })
});

router.delete('/', validation.validatePost(validation.get) ,function (req, res, next) {

    DB.readFile(DB.fullMainFilePath)
        .then(function (data) {
            debugger;
            var parsedData = JSON.parse(data.content);
            var result = DB.deleteFromJsonArr(parsedData.Docs,req.body.id);
            if(!result)  throw new Error("No data");
            parsedData.Docs = result;
            var newObject = {};
            newObject.path = DB.fullMainFilePath;
            newObject.content = parsedData;
            return newObject;

        })
        .then(DB.writeFile)
        .then(function (data) {

            res.json(data);
            res.end();
            //index after response sent
            DB.index(data.content.Docs);
        })
        .catch(function (err) {
            debugger;
            if(err.message == "No data"){
                return res.status(204).end();
            }else{
                next(err)
            }

        });

});


















module.exports = router;
