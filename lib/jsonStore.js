/**
 * Created by serkandemirel on 12/08/16.
 */
var fs = require('fs');
var path = require('path');


function JsonDB() {
    this.storePath = "./Store/";
    this.defaultMainFile = "/main.json";
    this.fullStorePath = path.resolve(__dirname, this.storePath);
    this.fullMainFilePath = path.resolve(__dirname, this.storePath + this.defaultMainFile);
    this.defaultConfigFileContent = {"TotalCount": 0, "Docs": []};
    this.defaultemptyData = {"Id": 8, "title": null, "body": null, "timeStamp": null};
    this.indexedArray = [];
}


JsonDB.prototype = {


    index: function (obj) {

        for (var i in obj) {
            this.indexedArray.push(i);
        }

    },

    uniqueId: function uniqueId() {
        var date = Date.now();

        // If created at same millisecond as previous
        if (date <= uniqueId.previous) {
            date = ++uniqueId.previous;
        } else {
            uniqueId.previous = date;
        }

        return date;
    },

    searchInJsonArr : function search(data,id) {
        for(var i = 0; i < data.length; i++)
        {
            if(data[i].id == id)
            {
                return data[i];
            }
        }
        return null;
    },

    updateJsonArr : function update(data ,id, newValuesj) {

        for(var i = 0; i < data.length; i++)
        {
            if(data[i].id == id)
            {
                data[i].title = newValuesj.title;
                data[i].body = newValuesj.body;
                data[i].timestamp = new Date().getTime();
                return data;
            }
        }
        return null;
    },

    deleteFromJsonArr : function del(data ,id) {

        for(var i = 0; i < data.length; i++)
        {
            if(data[i].id == id)
            {
                data.splice(i,1);
                return data;
            }
        }
        return null;
    },

    //region Async file read, returns promise
    readFile: function (path) {

        var promise = new Promise(function (resolve, reject) {

            fs.readFile(path, 'utf8', function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve({status: "success", "path": path, "content": data});
                }
            });

        });
        return promise;
    },
    //endregion

    //region Async file write, returns promise
    writeFile: function (param) {

        var promise = new Promise(function (resolve, reject) {

            fs.writeFile(param.path, JSON.stringify(param.content), function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve({status: "success", "path": param.path, "content": param.content});
                }
            });
        });
        return promise;

    },
    //endregion


}

module.exports = JsonDB;

