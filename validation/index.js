var Joi = require("joi");


module.exports.post = {
    title: Joi.string().required(),
    body: Joi.string().required()
};

module.exports.get = {
    id:  Joi.number().integer().required()
};

module.exports.put = {
    id:  Joi.number().integer().required(),
    title: Joi.string().required(),
    body: Joi.string().required()
};




module.exports.validatePost = function validate(validateSchema) {

    return function (req, res, next) {
        Joi.validate( req.body,
            validateSchema,
            function (err, value) {
                if (err) {
                    return res.send(err);
                }
                next();
            }
        );

    }
}


module.exports.validateGet = function validate(validateSchema) {

    return function (req, res, next) {
        Joi.validate( req.params,
            validateSchema,
            function (err, value) {
                if (err) {
                    return res.send(err);
                }
                next();
            }
        );

    }
}













