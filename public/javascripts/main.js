function Article() {
    this.baseUrl = "http://localhost:3000/api/article/all"
    this._article = undefined;
}

// define setter and getter methods for the property name
Object.defineProperty(Article.prototype, "article", {
    set: function (val) {
        // save the value to the cache variable
        this._article = val;
        // run_listener_function_here()
        alert("New value: " + val);
    },
    get: function () {
        // return value from the cache variable
        return this._article;
    }
});






function init(data) {

        $.ajax({
            url: this.baseUrl,
            data: null
        })
            .done(function (data) {
                data.article =  data;
            })
            .fail(function () {
                alert("error");
            })
            .always(function () {
                alert("complete");
            });

}



var rt = new Article();
init(rt);

$(document).ready(function () {

});